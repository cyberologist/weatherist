import 'package:flutter/material.dart';
import 'package:weatherist/src/presentation/screens/splash/splash.dart';

import 'src/core/injector/injection_container.dart' as injection_container;

void main() async {
  await injection_container.init();
  runApp(App());
}

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: Color(0xff08808E),
        primaryColorDark: Color(0xff055059),
        primaryColorLight: Color(0xff7AC9D0),
        accentColor: Color(0xffFe9A84),
        canvasColor: Color(0xffC8E8EA),
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: SplashScreen(),
    );
  }
}
