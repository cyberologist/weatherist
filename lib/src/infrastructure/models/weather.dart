import 'package:weatherist/src/domain/entities/weather.dart';

import 'model.dart';

class WeatherDetailsModel extends WeatherDetails implements Model {
  WeatherDetailsModel(
    Weather condition,
    double temprature,
    double windSpeed,
    double humidity,
    String description,
    String city,
    DateTime date,
  ) : super(
          condition,
          temprature,
          windSpeed,
          humidity,
          description,
          city,
          date,
        );

  factory WeatherDetailsModel.fromJson(Map<String, dynamic> data) {
    DateTime timeWithHours =
        DateTime.fromMillisecondsSinceEpoch(data['dt'] * 1000);
    return WeatherDetailsModel(
      _stringToWeatherCondition(data['weather'][0]['main']),
      0.0 + data['main']['temp'],
      0.0 + data['wind']['speed'],
      0.0 + data['main']['humidity'],
      data['weather'][0]['description'],
      data['city']['name'],
      DateTime(
        timeWithHours.year,
        timeWithHours.month,
        timeWithHours.day,
      ),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'weather': [
        {
          'description': description,
          'main': _weatherConditionToString(condition)
        }
      ],
      'main': {'temp': temprature, 'humidity': humidity},
      'wind': {'speed': windSpeed},
      'city': {'name': city},
      'dt': date.millisecondsSinceEpoch,
    };
  }

  factory WeatherDetailsModel.fromEntity(WeatherDetails data) {
    return WeatherDetailsModel(
      data.condition,
      data.temprature,
      data.windSpeed,
      data.humidity,
      data.description,
      data.city,
      data.date,
    );
  }
  static Weather _stringToWeatherCondition(String main) {
    if (main == 'Clear') {
      return Weather.Clear;
    } else if (main == 'Rain') {
      return Weather.Rain;
    } else if (main == 'Snow') {
      return Weather.Snow;
    } else if (main == 'Drizzle') {
      return Weather.Drizzle;
    } else if (main == 'Clouds') {
      return Weather.Clouds;
    } else if (main == 'ThunderStorm') {
      return Weather.Thunderstorm;
    } else {
      return Weather.Clear;
    }
  }

  static String _weatherConditionToString(Weather condition) {
    switch (condition) {
      case Weather.Clear:
        return 'Clear';
      case Weather.Clouds:
        return 'Rain';
      case Weather.Snow:
        return 'Snow';
      case Weather.Rain:
        return 'Drizzle';
      case Weather.Drizzle:
        return 'Clouds';
      case Weather.Thunderstorm:
        return 'ThunderStorm';
      default:
        return "Clear";
    }
  }
}
