import 'package:weatherist/src/domain/entities/weather-list.dart';
import 'package:weatherist/src/domain/entities/weather.dart';
import 'package:weatherist/src/infrastructure/models/weather.dart';

import 'model.dart';

class WeatherListModel extends WeatherList implements Model {
  WeatherListModel(List<WeatherDetails> weatherDays) : super(weatherDays);

  factory WeatherListModel.fromJson(Map<String, dynamic> data) {
    int index = 0;
    // filternig the list to get only unique days
    // this is becaue an api limitation
    dynamic temp = (data['list'] as List<dynamic>).where((value) {
      return (data['list']).toList().indexWhere((now) {
            String date1 = now['dt_txt'].split(' ')[0];
            String date2 = value['dt_txt'].split(' ')[0];
            return date1 == date2;
          }) ==
          index++;
    }).toList();

    temp = temp
        .toList()
        .map(
          (weatherDay) => weatherDay..putIfAbsent('city', () => data['city']),
        )
        .toList()
        .cast<Map<String, dynamic>>();
    return WeatherListModel(
      <WeatherDetails>[
        WeatherDetailsModel.fromJson(temp[0]),
        WeatherDetailsModel.fromJson(temp[1]),
        WeatherDetailsModel.fromJson(temp[2])
      ],
    );
  }
}
