import 'dart:convert';

import 'package:hive/hive.dart';
import 'package:weatherist/src/core/constants/config.dart';
import 'package:weatherist/src/core/extension.dart';
import 'package:weatherist/src/domain/entities/weather.dart';
import 'package:weatherist/src/domain/facedes/i_local_data_source.dart';
import 'package:weatherist/src/infrastructure/exceptions/data_not_found_exception.dart';
import 'package:weatherist/src/infrastructure/exceptions/parse_exception.dart';
import 'package:weatherist/src/infrastructure/models/weather.dart';

class LocalDataSource implements ILocalDataSource {
  Box weatherBox;
  LocalDataSource() : weatherBox = Hive.box('WEATHER');

  @override
  Future<bool> checkWeatherExists(
    DateTime date,
    double lat,
    double long,
    String lang,
  ) async {
    Box weatherBox = await Hive.openBox(WEATHER_BOX_KEY);
    return weatherBox.containsKey(_encodeWeatherKey(date, lat, long, lang));
  }

  @override
  Future<bool> setWeatherDetails(
    WeatherDetails weatherDetails,
    double lat,
    double long,
    String lang,
  ) async {
    String weatherKey = _encodeWeatherKey(weatherDetails.date, lat, long, lang);

    WeatherDetailsModel weatherModel =
        WeatherDetailsModel.fromEntity(weatherDetails);

    Map<String, dynamic> weatherJson = weatherModel.toJson();

    String weatherString = json.encode(weatherJson);

    weatherBox.put(weatherKey, weatherString);

    optimizeCashUntil(DateTime.now());

    return true;
  }

  @override
  Future<WeatherDetails> getWeatherByDateTime(
    DateTime date,
    double lat,
    double long,
    String lang,
  ) async {
    String weatherKey = _encodeWeatherKey(date, lat, long, lang);

    String weatherDetailsString = weatherBox.get(weatherKey);

    if (weatherDetailsString == null || weatherDetailsString.length == 0) {
      throw DataNotFoundException();
    } else {
      try {
        Map<String, dynamic> weatherJson = json.decode(weatherDetailsString);

        return WeatherDetailsModel.fromJson(weatherJson);
      } catch (e) {
        throw ParseException();
      }
    }
  }

  @override
  Future<void> optimizeCashUntil(DateTime clearUntil) async {
    for (int i = 0; i < weatherBox.length; i++) {
      String currentTimeKey = weatherBox.keyAt(i);
      DateTime currentTime = _decodeDateFromWeatherKey(currentTimeKey);
      if (currentTime.isBefore(clearUntil.subtract(Duration(days: 1)))) {
        weatherBox.deleteAt(i);
      }
    }
    await weatherBox.compact();
  }

  String _encodeWeatherKey(
      DateTime date, double lat, double long, String lang) {
    return '_${date.year}_${date.month}_${date.day}_${lat.toPrecision(2)}_${long.toPrecision(2)}_${lang}_';
  }

  DateTime _decodeDateFromWeatherKey(String key) {
    List<String> currentTimeStringArray = key.split('_');
    return DateTime(
      int.parse(currentTimeStringArray[1]),
      int.parse(currentTimeStringArray[2]),
      int.parse(currentTimeStringArray[3]),
    );
  }
}
