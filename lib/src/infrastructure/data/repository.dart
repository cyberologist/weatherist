import 'package:dartz/dartz.dart';
import 'package:weatherist/src/core/constants/consts.dart';
import 'package:weatherist/src/domain/entities/weather-list.dart';
import 'package:weatherist/src/domain/entities/weather.dart';
import 'package:weatherist/src/domain/facedes/i_local_data_source.dart';
import 'package:weatherist/src/domain/facedes/i_remote_data_source.dart';
import 'package:weatherist/src/domain/facedes/i_repository.dart';
import 'package:weatherist/src/domain/failures/data_not_found_failure.dart';
import 'package:weatherist/src/domain/failures/failure.dart';
import 'package:weatherist/src/domain/failures/network_failure.dart';
import 'package:weatherist/src/domain/failures/server_failure.dart';
import 'package:weatherist/src/domain/failures/unknwon_failure.dart';
import 'package:weatherist/src/infrastructure/exceptions/no_internet_exception.dart';
import 'package:weatherist/src/infrastructure/exceptions/parse_exception.dart';
import 'package:weatherist/src/infrastructure/exceptions/server_exception.dart';
import 'package:weatherist/src/infrastructure/exceptions/unknown_exception.dart';

class Repository implements IRepository {
  final IRemoteDataSource _remoteDataSource;
  final ILocalDataSource _localDataSource;

  Repository(this._remoteDataSource, this._localDataSource);

  @override
  Future<Either<Failure, WeatherDetails>> getWeatherDetails(
    DateTime date, {
    double lat = DEFAULT_LAT,
    double long = DEFAULT_LONG,
    String lang = DEFAULT_LANGUAGE,
  }) async {
    bool exists =
        await _localDataSource.checkWeatherExists(date, lat, long, lang);
    if (exists) {
      return await _fetchFromCache(date, lat, long, lang);
    } else {
      return await _fetchFromRemoteApi(date, lat, long, lang);
    }
  }

  Future<Either<Failure, WeatherDetails>> _fetchFromCache(
    DateTime date,
    double lat,
    double long,
    String lang,
  ) async {
    return Right(
      await _localDataSource.getWeatherByDateTime(
        date,
        lat,
        long,
        lang,
      ),
    );
  }

  Future<Either<Failure, WeatherDetails>> _fetchFromRemoteApi(
    DateTime date,
    double lat,
    double long,
    String lang,
  ) async {
    try {
      WeatherList weatherList = await _remoteDataSource.getNextThreeDaysWeather(
        lat,
        long,
        lang,
      );
      _cacheWeatherList(weatherList, lat, long, lang);

      return Right(
          weatherList.weatherDays.firstWhere((WeatherDetails weatherDetails) {
        return weatherDetails.date.isAtSameMomentAs(date);
      }));
    } on StateError {
      return Left(DataNotFoundFailure());
    } on NoInternetException {
      return Left(NetworkFailure());
    } on UnknownException {
      return Left(UnknwonFailure());
    } on ParseException {
      return Left(ServerFailure());
    } on ServerException {
      return Left(ServerFailure());
    } catch (_) {
      return Left(UnknwonFailure());
    }
  }

  _cacheWeatherList(
      WeatherList weatherList, double lat, double long, String lang) {
    weatherList.weatherDays.forEach(
      (weatherDay) => _localDataSource.setWeatherDetails(
        weatherDay,
        lat,
        long,
        lang,
      ),
    );
  }
}
