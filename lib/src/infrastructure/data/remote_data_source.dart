import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart';
import 'package:weatherist/src/core/constants/config.dart';
import 'package:weatherist/src/core/constants/consts.dart';
import 'package:weatherist/src/domain/facedes/i_remote_data_source.dart';
import 'package:weatherist/src/infrastructure/exceptions/no_internet_exception.dart';
import 'package:weatherist/src/infrastructure/exceptions/parse_exception.dart';
import 'package:weatherist/src/infrastructure/exceptions/server_exception.dart';
import 'package:weatherist/src/infrastructure/exceptions/unknown_exception.dart';
import 'package:weatherist/src/infrastructure/models/weather-list.dart';

class RemoteDataSource extends IRemoteDataSource {
  final Client _client;
  RemoteDataSource() : this._client = Client();

  @override
  Future<WeatherListModel> getNextThreeDaysWeather(
      double lat, double long, String lang) async {
    final Map<String, String> queryParameters = {
      LAT_QUERY_NAME: '$lat',
      LONG_QUERY_NAME: '$long',
      LAND_QUERY_NAME: lang,
      CNT_QUERY_NAME: DEFAULT_CNT,
      EXECLUDE_QUERY_NAME: DEFAULT_EXECLUDE,
      API_KEY_QUERY_NAME: API_KEY,
    };

    final Uri url = Uri.http(
      REMOTE_API_HOST,
      REMOTE_API_PATH,
      queryParameters,
    );
    Response response;

    try {
      response = await _client.get(url);
    } on SocketException {
      throw NoInternetException();
    } catch (_) {
      throw UnknownException();
    }

    return _responseProccessor<WeatherListModel>(
      response,
      (data) => WeatherListModel.fromJson(data),
    );
  }

  T _responseProccessor<T>(Response response, parser) {
    if (response.statusCode == 200) {
      try {
        return parser(json.decode(response.body));
      } catch (_) {
        throw ParseException();
      }
    } else if (response.statusCode == 500) {
      throw ServerException();
    } else {
      throw UnknownException();
    }
  }
}
