import 'package:dartz/dartz.dart';
import 'package:weatherist/src/application/core/usecase.dart';
import 'package:weatherist/src/core/constants/messages.dart';
import 'package:weatherist/src/domain/failures/failure.dart';

class FetchThreeDaysDates
    extends Usecase<List<Tuple2<String, String>>, NoParams> {
  @override
  Future<Either<Failure, List<Tuple2<String, String>>>> call(
      NoParams params) async {
    await Future.delayed(Duration(seconds: 4), () {});
    DateTime date = DateTime.now();
    return Right(
      <Tuple2<String, String>>[
        Tuple2<String, String>(
          _formateDate(date),
          TODAY,
        ),
        Tuple2<String, String>(
          _formateDate(date.add(Duration(days: 1))),
          TOMOROW,
        ),
        Tuple2<String, String>(
          _formateDate(date.add(Duration(days: 2))),
          IN_TWO_DAYS,
        ),
      ],
    );
  }

  String _formateDate(DateTime date) {
    return date.toLocal().toString().split(' ')[0].replaceAll('-', '/');
  }
}
