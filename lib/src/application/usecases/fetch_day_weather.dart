import 'package:dartz/dartz.dart';
import 'package:weatherist/src/application/core/usecase.dart';
import 'package:weatherist/src/core/constants/consts.dart';
import 'package:weatherist/src/domain/entities/weather.dart';
import 'package:weatherist/src/domain/facedes/i_repository.dart';
import 'package:weatherist/src/domain/failures/failure.dart';

class FetchDayWeather extends Usecase<WeatherDetails, FetchDayWeatherParams> {
  final IRepository _repository;

  FetchDayWeather(this._repository);
  @override
  Future<Either<Failure, WeatherDetails>> call(FetchDayWeatherParams params) {
    return _repository.getWeatherDetails(
      decodeDateString(params.date),
      lat: params.long,
      long: params.lat,
      lang: params.lang,
    );
  }

  DateTime decodeDateString(String date) {
    List<int> dateArray = date.split("/").map((d) => int.parse(d)).toList();
    return DateTime(dateArray[0], dateArray[1], dateArray[2]);
  }
}

class FetchDayWeatherParams {
  final String date;
  final double long;
  final double lat;
  final String lang;

  FetchDayWeatherParams(
    this.date, {
    this.lat = DEFAULT_LAT,
    this.long = DEFAULT_LONG,
    this.lang = DEFAULT_LANGUAGE,
  });
}
