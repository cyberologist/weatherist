import 'package:dartz/dartz.dart' hide State;
import 'package:flutter/material.dart';
import 'package:weatherist/src/presentation/screens/core/slide.dart';
import 'package:weatherist/src/presentation/screens/weather/weather.dart';

class DateButton extends StatefulWidget {
  final Tuple2<String, String> date;

  const DateButton(this.date, {Key key}) : super(key: key);
  @override
  _DateButtonState createState() => _DateButtonState();
}

class _DateButtonState extends State<DateButton>
    with SingleTickerProviderStateMixin {
  double scale;
  AnimationController _controller;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 100),
      lowerBound: 0.0,
      upperBound: 0.1,
    )..addListener(() => setState(() {}));
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  void _onTapDown(TapDownDetails details) {
    _controller.forward();
  }

  void _onTapUp(TapUpDetails details) {
    _controller.reverse();
  }

  void _onTapCancell() {
    _controller.reverse();
  }

  void onTap() {
    Navigator.of(context).push(
      SlideRightRoute(
        page: WeatherDetailsScreen(
          widget.date.value1,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    scale = 1 - _controller.value;
    return Transform.scale(
      scale: scale,
      child: Transform.rotate(
        angle: 1.0 * (1 - scale),
        child: Container(
          margin: EdgeInsets.all(15),
          decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                color: Theme.of(context).primaryColor,
                offset: Offset(0, 15),
                blurRadius: 20,
              ),
            ],
          ),
          child: GestureDetector(
            onTap: onTap,
            onTapDown: _onTapDown,
            onTapUp: _onTapUp,
            onTapCancel: _onTapCancell,
            child: Material(
              color: Colors.transparent,
              child: AnimatedContainer(
                duration: Duration(milliseconds: 500),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(
                    Radius.circular(40),
                  ),
                  gradient: LinearGradient(
                    colors: getColorsList(),
                  ),
                ),
                padding: EdgeInsets.all(15),
                child: Column(
                  children: [
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Container(
                        padding: EdgeInsets.all(10),
                        child: Text(
                          widget.date.value2,
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            fontFamily: 'Aeonik-Medium',
                            fontSize: 32,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 5),
                    Align(
                      alignment: Alignment.centerRight,
                      child: Container(
                        padding: EdgeInsets.all(20),
                        child: Text(
                          widget.date.value1,
                          textAlign: TextAlign.right,
                          style: TextStyle(
                            fontFamily: 'Aeonik-Light',
                            fontSize: 24,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  List<Color> getColorsList() {
    List<Color> list = <Color>[
      Theme.of(context).primaryColorDark,
      Theme.of(context).primaryColorLight,
      Theme.of(context).primaryColor,
      Theme.of(context).accentColor,
      Theme.of(context).canvasColor.withAlpha(100),
    ]..shuffle();
    return list.sublist(0, 2);
  }
}
