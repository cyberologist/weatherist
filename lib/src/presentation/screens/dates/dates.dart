import 'package:dartz/dartz.dart' hide State;
import 'package:flutter/material.dart';
import 'package:weatherist/src/core/constants/messages.dart';
import 'package:weatherist/src/presentation/screens/dates/widget/date.dart';

class DatesScreen extends StatefulWidget {
  final List<Tuple2<String, String>> dates;
  DatesScreen(this.dates);
  @override
  _DatesScreenState createState() => _DatesScreenState();
}

class _DatesScreenState extends State<DatesScreen> {
  @override
  Widget build(BuildContext context) {
    return Material(
      color: Theme.of(context).primaryColorDark,
      child: ListView(
        physics: BouncingScrollPhysics(),
        children: [
          SizedBox(height: 10),
          Center(
            child: Text(
              WELCOME,
              style: TextStyle(
                fontFamily: 'Signatra',
                color: Colors.white,
                fontSize: 48.0,
              ),
            ),
          ),
          // buildLightLine(),
          Container(
            margin: EdgeInsets.all(20),
            child: RichText(
              text: TextSpan(
                text: TAP_PROMPT_PT1,
                children: [
                  TextSpan(
                    text: TAP_PROMPT_PT2,
                    style: TextStyle(
                      fontFamily: "Aeonik-Medium",
                      fontSize: 32,
                    ),
                  ),
                ],
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 28,
                  fontFamily: "Aeonik-Light",
                ),
              ),
            ),
          ),
          buildLightLine(),
          ...widget.dates
              .map(
                (date) => DateButton(date),
              )
              .toList()
              .cast<Widget>()
        ],
      ),
    );
  }

  Container buildLightLine() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 35),
      height: 2,
      width: double.infinity,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: [
            Colors.white.withOpacity(0.1),
            Colors.white,
            Colors.white.withOpacity(0.1),
          ],
        ),
      ),
    );
  }
}
