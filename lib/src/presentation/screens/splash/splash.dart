import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:weatherist/src/core/injector/injection_container.dart';
import 'package:weatherist/src/presentation/blocs/dates/dates_bloc.dart';
import 'package:weatherist/src/presentation/screens/core/fade.dart';
import 'package:weatherist/src/presentation/screens/core/responsive_safe_area.dart';
import 'package:weatherist/src/presentation/screens/dates/dates.dart';
import 'package:weatherist/src/presentation/screens/splash/widgets/app_logo.dart';
import 'package:weatherist/src/presentation/screens/splash/widgets/background.dart';
import 'package:weatherist/src/presentation/screens/splash/widgets/cloud_header.dart';
import 'package:weatherist/src/presentation/screens/splash/widgets/dev_logo.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with TickerProviderStateMixin {
  DatesBloc bloc;
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    bloc = getIt<DatesBloc>();
    bloc.add(FetchDatesTriggerd());
  }

  @override
  Widget build(BuildContext context) {
    return ResponsiveSafeArea(builder: (context, size) {
      return BlocListener<DatesBloc, DatesState>(
        cubit: bloc,
        listener: (context, state) {
          if (state is DatesLoaded) {
            Navigator.of(context).pushAndRemoveUntil(
              FadeRoute(page: DatesScreen(state.dates)),
              (route) => false,
            );
          } else if (state is DatesFailedToLoad) {
            Flushbar(
              title: state.message,
              backgroundColor: Colors.red,
              boxShadows: [
                BoxShadow(
                  color: Colors.red[800],
                  offset: Offset(0.0, 2.0),
                  blurRadius: 3.0,
                )
              ],
            )..show(context);
          }
        },
        child: Stack(children: [
          SplashBackground(),
          Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                CloudHeader(size.height, size.width),
                AppLogo(size.height),
                DevLogo(size.height),
              ]),
        ]),
      );
    });
  }
}
