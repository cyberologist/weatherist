import 'package:animator/animator.dart';
import 'package:flutter/material.dart';
import 'package:weatherist/src/core/constants/messages.dart';

class AppLogo extends StatelessWidget {
  final double height;
  AppLogo(this.height);
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Material(
        color: Colors.transparent,
        child: Container(
          height: height * 0.3,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Animator(
                resetAnimationOnRebuild: true,
                duration: Duration(seconds: 2),
                tween: Tween(begin: 0.0, end: 1.0),
                curve: Curves.easeIn,
                builder: (context, animation, widget) {
                  return Opacity(
                    opacity: 0.0 + (animation.value),
                    child: Column(
                      children: [
                        Text(
                          APP_NAME,
                          style: TextStyle(
                            fontFamily: 'Aeonik-Medium',
                            color: Colors.white,
                            fontSize: height * 0.0015 * 48.0,
                          ),
                        ),
                        SizedBox(height: 10),
                        Text(
                          APP_SLOGAN,
                          style: TextStyle(
                            fontFamily: 'Aeonik-Light',
                            color: Colors.white,
                            fontSize: height * 0.0015 * 18.0,
                          ),
                        ),
                      ],
                    ),
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
