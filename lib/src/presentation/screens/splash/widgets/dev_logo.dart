import 'package:animator/animator.dart';
import 'package:flutter/material.dart';
import 'package:weatherist/src/core/constants/messages.dart';

class DevLogo extends StatelessWidget {
  final double height;
  DevLogo(this.height);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height * 0.3,
      child: Material(
        color: Colors.transparent,
        child: Animator(
            resetAnimationOnRebuild: true,
            duration: Duration(milliseconds: 1000),
            tween: Tween(begin: 0.0, end: 1.0),
            builder: (context, animation, widget) {
              return Opacity(
                opacity: 0.0 + animation.value,
                child: Container(
                  padding: EdgeInsets.all(10),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        FROM,
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: height * 0.0015 * 12,
                          letterSpacing: 4,
                        ),
                      ),
                      SizedBox(height: 1.0),
                      Text(
                        DEV_COMPANY_NAME,
                        style: TextStyle(
                          fontFamily: 'Signatra',
                          color: Theme.of(context).backgroundColor,
                          fontSize: height * 0.0015 * 48,
                        ),
                      ),
                      SizedBox(height: 1.0),
                      Text(
                        MY_NAME,
                        style: TextStyle(
                          fontFamily: 'Aeonik-Light',
                          color: Colors.white,
                          fontSize: height * 0.0015 * 18,
                        ),
                      )
                    ],
                  ),
                ),
              );
            }),
      ),
    );
  }
}
