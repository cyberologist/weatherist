import 'package:animator/animator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class CloudHeader extends StatefulWidget {
  final double height;
  final double width;
  CloudHeader(this.height, this.width);
  @override
  _CloudHeaderState createState() => _CloudHeaderState();
}

class _CloudHeaderState extends State<CloudHeader> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: widget.height * 0.3,
      child: Animator(
          resetAnimationOnRebuild: true,
          tween: Tween(begin: 0.0, end: 1.0),
          duration: Duration(seconds: 5),
          curve: Curves.easeIn,
          cycles: 4,
          builder: (context, animation, cachedWidget) {
            return FractionalTranslation(
              translation: Offset(0.0 + animation.value, 0),
              child: Container(
                margin: EdgeInsets.only(top: 20),
                child: SvgPicture.asset(
                  './assets/svgs/cloudy.svg',
                  color: Colors.white,
                  width: widget.width * 0.2,
                  height: widget.height * 0.2,
                ),
              ),
            );
          }),
    );
  }
}
