import 'package:flutter/material.dart';
import 'package:wave/config.dart';
import 'package:wave/wave.dart';

class SplashBackground extends StatefulWidget {
  @override
  _SplashBackgroundState createState() => _SplashBackgroundState();
}

class _SplashBackgroundState extends State<SplashBackground> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      child: RotatedBox(
        quarterTurns: 2,
        child: WaveWidget(
          config: CustomConfig(
            gradients: [
              [
                Theme.of(context).primaryColor,
                Theme.of(context).primaryColorLight
              ],
              [
                Theme.of(context).accentColor,
                Theme.of(context).primaryColorDark,
                Theme.of(context).primaryColorDark,
              ],
            ],
            durations: [19440, 10800],
            heightPercentages: [0.20, 0.25],
            blur: MaskFilter.blur(BlurStyle.solid, 10),
            gradientBegin: Alignment.bottomLeft,
            gradientEnd: Alignment.topRight,
          ),
          waveAmplitude: 2,
          size: Size(
            double.infinity,
            double.infinity,
          ),
        ),
      ),
    );
  }
}
