import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:weatherist/src/domain/entities/weather.dart';

class WeatherImageError extends StatelessWidget {
  final double height;
  WeatherImageError(this.height);
  @override
  Widget build(BuildContext context) {
    return Container(
      height: height * 0.5,
      child: FlareActor(
        "assets/animations/Loading_error_and_check.flr",
        alignment: Alignment.center,
        fit: BoxFit.contain,
        animation: "Error",
      ),
    );
  }
}

class WeatherImageLoaded extends StatelessWidget {
  final double height;
  WeatherImageLoaded(this.height);
  @override
  Widget build(BuildContext context) {
    return Container(
      height: height * 0.5,
      child: FlareActor(
        "assets/animations/Loading_error_and_check.flr",
        alignment: Alignment.center,
        fit: BoxFit.contain,
        animation: "Success",
      ),
    );
  }
}

class WeatherLoadingg extends StatelessWidget {
  final double height;
  WeatherLoadingg(this.height);
  @override
  Widget build(BuildContext context) {
    return Container(
      height: height * 0.5,
      child: FlareActor(
        "assets/animations/Loading_error_and_check.flr",
        alignment: Alignment.center,
        fit: BoxFit.contain,
        animation: "Loading",
      ),
    );
  }
}

class WeatherImage extends StatelessWidget {
  final double height;
  final Weather condition;

  const WeatherImage(this.height, this.condition, {Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    String animationName;
    switch (condition) {
      case Weather.Clear:
        animationName = '01d';
        break;
      case Weather.Clouds:
        animationName = '03d';
        break;
      case Weather.Snow:
        animationName = '13d';
        break;
      case Weather.Rain:
        animationName = '09d';
        break;
      case Weather.Drizzle:
        animationName = '10d';
        break;
      case Weather.Thunderstorm:
        animationName = '11d';
        break;
    }
    return Container(
      height: height * 0.5,
      child: FlareActor(
        "assets/animations/weather_icons.flr",
        alignment: Alignment.center,
        fit: BoxFit.contain,
        animation: animationName,
      ),
    );
  }
}
