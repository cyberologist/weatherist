import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
import 'package:weatherist/src/core/constants/messages.dart';
import 'package:weatherist/src/domain/entities/weather.dart';
import '../../../../core/extension.dart';

class DetailsLoaded extends StatelessWidget {
  final double height;
  final WeatherDetails details;
  DetailsLoaded(this.height, this.details);
  @override
  Widget build(BuildContext context) {
    var boxDecoration = BoxDecoration(
      color: Colors.white,
      borderRadius: BorderRadius.circular(2),
    );
    return Container(
      height: height * 0.3,
      padding: EdgeInsets.only(left: 20, top: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            child: Text(
              details.city,
              style: TextStyle(
                fontSize: height * 0.0015 * 28,
                fontFamily: "Aeonik-Light",
                color: Colors.white,
              ),
            ),
          ),
          SizedBox(height: 15),
          Container(
            height: height * 0.17,
            child: ListView(
                physics: BouncingScrollPhysics(),
                scrollDirection: Axis.horizontal,
                children: [
                  Container(
                    height: height * 0.17,
                    child: Center(
                      child: Text(
                        '${toCelsius(details.temprature)}°',
                        style: TextStyle(
                          fontSize: height * 0.0015 * 64,
                          fontFamily: "Aeonik-Light",
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(width: 20),
                  buildDetailsBox(
                    boxDecoration,
                    WIND,
                    '${details.windSpeed}$KM_PER_HOUR',
                  ),
                  SizedBox(width: 20),
                  buildDetailsBox(
                    boxDecoration,
                    HUMIDITY,
                    '${details.humidity}%     ',
                  ),
                ]),
          ),
          SizedBox(height: 15),
        ],
      ),
    );
  }

  Widget buildDetailsBox(
    BoxDecoration boxDecoration,
    String name,
    String value,
  ) {
    return ConstrainedBox(
      constraints: new BoxConstraints(
        minHeight: height * 0.14,
        minWidth: 150,
        maxHeight: height * 0.14,
        maxWidth: 500.0,
      ),
      child: Container(
        padding: EdgeInsets.all(20),
        decoration: boxDecoration,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              name,
              style: TextStyle(
                color: Colors.black26,
                fontFamily: "Aeonik-Light",
                fontSize: height * 0.0015 * 20,
              ),
            ),
            SizedBox(height: 5),
            Text(
              value,
              style: TextStyle(
                color: Colors.black54,
                fontFamily: "Aeonik-Regular",
                fontSize: height * 0.0015 * 28,
              ),
            ),
          ],
        ),
      ),
    );
  }

  double toCelsius(double fehrenhite) {
    return (fehrenhite - 273.15).toPrecision(1);
  }
}

class DetailsLoadingError extends StatelessWidget {
  final double height;
  final String errorMessage;
  DetailsLoadingError(this.height, this.errorMessage);
  @override
  Widget build(BuildContext context) {
    return Container(
      height: height * 0.3,
      padding: EdgeInsets.only(left: 20, top: 30),
      child: Text(
        errorMessage,
        textAlign: TextAlign.center,
        style: TextStyle(
          color: Colors.redAccent,
          fontFamily: "Aeonik-Light",
          fontSize: height * 0.0015 * 32,
        ),
      ),
    );
  }
}

class DetailsLoading extends StatelessWidget {
  final double height;
  DetailsLoading(this.height);

  @override
  Widget build(BuildContext context) {
    var boxDecoration = BoxDecoration(
      color: Colors.white,
      borderRadius: BorderRadius.circular(2),
    );
    return Container(
      height: height * 0.3,
      padding: EdgeInsets.only(left: 20, top: 30),
      child: Shimmer.fromColors(
        baseColor: Theme.of(context).canvasColor,
        highlightColor: Theme.of(context).primaryColorLight,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              width: 150,
              height: height * 0.03,
              decoration: boxDecoration,
            ),
            SizedBox(height: 15),
            Container(
              height: height * 0.17,
              child: ListView(
                  physics: BouncingScrollPhysics(),
                  scrollDirection: Axis.horizontal,
                  children: [
                    Container(
                      height: height * 0.19,
                      width: 150,
                      decoration: boxDecoration,
                    ),
                    SizedBox(width: 20),
                    Container(
                      height: height * 0.19,
                      width: 150,
                      decoration: boxDecoration,
                    ),
                    SizedBox(width: 20),
                    Container(
                      height: height * 0.19,
                      width: 150,
                      decoration: boxDecoration,
                    ),
                  ]),
            )
          ],
        ),
      ),
    );
  }
}
