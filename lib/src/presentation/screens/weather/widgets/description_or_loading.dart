import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
import 'package:weatherist/src/core/constants/messages.dart';

class DescriptionWidget extends StatelessWidget {
  final String description;
  final double height;

  const DescriptionWidget(this.description, this.height, {Key key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height * 0.2,
      padding: EdgeInsets.only(left: 20, top: 30),
      child: RichText(
          text: TextSpan(
        children: description
            .split(' ')
            .map(
              (text) => TextSpan(
                text: text + '\n',
                style: TextStyle(
                  fontFamily: "Aeonik-Medium",
                  fontSize: height * 0.0015 * 36.0,
                ),
              ),
            )
            .toList()
            .cast<TextSpan>(),
      )),
    );
  }
}

class DescriptionLoading extends StatelessWidget {
  final double height;

  const DescriptionLoading(this.height, {Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      height: height * 0.2,
      padding: EdgeInsets.only(left: 20, top: 30),
      child: Shimmer.fromColors(
        baseColor: Theme.of(context).canvasColor,
        highlightColor: Theme.of(context).primaryColorLight,
        child: Text(
          LOADING_WEATHER,
          textAlign: TextAlign.left,
          style: TextStyle(
            fontSize: height * 0.0015 * 32.0,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }
}

class DescriptionError extends StatelessWidget {
  final double height;
  DescriptionError(this.height);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height * 0.2,
      padding: EdgeInsets.only(left: 20, top: 30),
      child: Text(
        ERROR,
        textAlign: TextAlign.left,
        style: TextStyle(
          fontSize: height * 0.0015 * 32.0,
          fontWeight: FontWeight.bold,
          color: Colors.redAccent,
        ),
      ),
    );
  }
}
