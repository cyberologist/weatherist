import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:weatherist/src/core/injector/injection_container.dart';
import 'package:weatherist/src/domain/entities/weather.dart';
import 'package:weatherist/src/presentation/blocs/weather/weather_bloc.dart';
import 'package:weatherist/src/presentation/screens/core/responsive_safe_area.dart';
import 'package:weatherist/src/presentation/screens/weather/widgets/description_or_loading.dart';
import 'package:weatherist/src/presentation/screens/weather/widgets/details_or_loading.dart';
import 'package:weatherist/src/presentation/screens/weather/widgets/weather_image_or_loading.dart';

class WeatherDetailsScreen extends StatefulWidget {
  final String date;

  const WeatherDetailsScreen(this.date, {Key key}) : super(key: key);
  @override
  _WeatherDetailsScreenState createState() => _WeatherDetailsScreenState();
}

enum LoadingState { Loading, Success, Error, Done }

class _WeatherDetailsScreenState extends State<WeatherDetailsScreen> {
  WeatherBloc bloc;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    bloc = getIt<WeatherBloc>();
    bloc.add(FetchDayWeatherClicked(widget.date));
  }

  LoadingState loadingStateEnum = LoadingState.Loading;
  WeatherDetails weatherDetails;
  String errorMessage;
  @override
  Widget build(BuildContext context) {
    return ResponsiveSafeArea(
      builder: (context, size) => BlocListener<WeatherBloc, WeatherState>(
        cubit: bloc,
        listener: (context, state) {
          if (state is WeatherLoading) {
          } else if (state is WeatherLoaded) {
            setState(() {
              weatherDetails = state.weatherDetails;
              loadingStateEnum = LoadingState.Done;
            });
          } else if (state is WeatherFailedToLoad) {
            setState(() {
              loadingStateEnum = LoadingState.Error;
              errorMessage = state.message;
            });
          } else {
            // impossible
          }
        },
        child: Material(
          color: conditionToColor(weatherDetails?.condition),
          child: ListView(
            children: [
              buildDescriptionToState(
                loadingStateEnum,
                size.height,
                description: weatherDetails?.description,
              ),
              buildConditionToState(
                loadingStateEnum,
                size.height,
                condition: weatherDetails?.condition,
              ),
              buildDetailsToState(
                loadingStateEnum,
                size.height,
                weatherDetails,
              ),
            ],
          ),
        ),
      ),
    );
  }

  Color conditionToColor(Weather condition) {
    if (condition == null) {
      return Colors.white;
    }
    switch (condition) {
      case Weather.Clear:
      case Weather.Clouds:
      case Weather.Snow:
      case Weather.Rain:
      case Weather.Drizzle:
      case Weather.Thunderstorm:
        return Theme.of(context).primaryColor;
        break;
    }
    return Colors.white;
  }

  Widget buildDescriptionToState(
    LoadingState loadingStateEnum,
    double height, {
    String description,
  }) {
    switch (loadingStateEnum) {
      case LoadingState.Loading:
        return DescriptionLoading(height);
        break;
      case LoadingState.Error:
        return DescriptionError(height);
        break;
      case LoadingState.Success:
      case LoadingState.Done:
        return DescriptionWidget(description, height);
        break;
    }
    //impossible
    return Container();
  }

  Widget buildDetailsToState(
    LoadingState loadingStateEnum,
    double height,
    WeatherDetails details,
  ) {
    switch (loadingStateEnum) {
      case LoadingState.Loading:
        return DetailsLoading(height);
        break;
      case LoadingState.Error:
        return DetailsLoadingError(height, errorMessage);
        break;
      case LoadingState.Success:
      case LoadingState.Done:
        return DetailsLoaded(height, details);
        break;
    }
    //impossible
    return Container();
  }

  Widget buildConditionToState(
    LoadingState loadingStateEnum,
    double height, {
    Weather condition,
  }) {
    switch (loadingStateEnum) {
      case LoadingState.Loading:
        return WeatherLoadingg(height);
        break;
      case LoadingState.Error:
        return WeatherImageError(height);
        break;
      case LoadingState.Success:
        return WeatherImageLoaded(height);
        break;
      case LoadingState.Done:
        return WeatherImage(height, condition);
        break;
    }
    //impossible
    return Container();
  }
}
