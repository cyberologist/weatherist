import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:weatherist/src/application/usecases/fetch_day_weather.dart';
import 'package:weatherist/src/domain/entities/weather.dart';

part 'weather_event.dart';
part 'weather_state.dart';

class WeatherBloc extends Bloc<WeatherEvent, WeatherState> {
  final FetchDayWeather _fetchDayWeather;
  WeatherBloc(this._fetchDayWeather) : super(WeatherInitial());

  @override
  Stream<WeatherState> mapEventToState(
    WeatherEvent event,
  ) async* {
    if (event is FetchDayWeatherClicked) {
      yield WeatherLoading();
      FetchDayWeatherParams params = FetchDayWeatherParams(event.date);
      final result = await _fetchDayWeather(params);
      yield result.fold(
        (failure) => WeatherFailedToLoad(failure.message),
        (weatherDetails) => WeatherLoaded(weatherDetails),
      );
    }
  }
}
