part of 'weather_bloc.dart';

@immutable
abstract class WeatherState {}

class WeatherInitial extends WeatherState {}

class WeatherLoaded extends WeatherState {
  final WeatherDetails weatherDetails;

  WeatherLoaded(this.weatherDetails);
}

class WeatherLoading extends WeatherState {}

class WeatherFailedToLoad extends WeatherState {
  final String message;

  WeatherFailedToLoad(this.message);
}
