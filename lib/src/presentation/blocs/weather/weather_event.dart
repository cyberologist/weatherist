part of 'weather_bloc.dart';

@immutable
abstract class WeatherEvent {}

class FetchDayWeatherClicked extends WeatherEvent {
  final String date;
  FetchDayWeatherClicked(this.date);
}
