import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';
import 'package:weatherist/src/application/core/usecase.dart';
import 'package:weatherist/src/application/usecases/fetch_three_days_dates.dart';

part 'dates_event.dart';
part 'dates_state.dart';

class DatesBloc extends Bloc<DatesEvent, DatesState> {
  final FetchThreeDaysDates _fetchThreeDaysDates;
  DatesBloc(this._fetchThreeDaysDates) : super(DatesInitial());

  @override
  Stream<DatesState> mapEventToState(
    DatesEvent event,
  ) async* {
    if (event is FetchDatesTriggerd) {
      yield DatesLoading();
      final result = await _fetchThreeDaysDates(NoParams());
      yield result.fold(
        (failure) => DatesFailedToLoad(failure.message),
        (dates) => DatesLoaded(dates),
      );
    }
  }
}
