part of 'dates_bloc.dart';

@immutable
abstract class DatesState {}

class DatesInitial extends DatesState {}

class DatesLoading extends DatesState {}

class DatesLoaded extends DatesState {
  final List<Tuple2<String, String>> dates;

  DatesLoaded(this.dates);
}

class DatesFailedToLoad extends DatesState {
  final String message;

  DatesFailedToLoad(this.message);
}
