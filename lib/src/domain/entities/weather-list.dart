import 'package:weatherist/src/domain/entities/weather.dart';

import 'entity.dart';

class WeatherList extends Entity {
  final List<WeatherDetails> weatherDays;

  WeatherList(this.weatherDays);
}
