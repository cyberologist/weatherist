import 'entity.dart';

enum Weather { Clear, Clouds, Snow, Rain, Drizzle, Thunderstorm }

class WeatherDetails extends Entity {
  final Weather condition;
  final double temprature;
  final double windSpeed;
  final double humidity;
  final String description;
  final String city;
  final DateTime date;
  WeatherDetails(
    this.condition,
    this.temprature,
    this.windSpeed,
    this.humidity,
    this.description,
    this.city,
    this.date,
  );
}
