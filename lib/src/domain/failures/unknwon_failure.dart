import 'package:weatherist/src/core/constants/messages.dart';

import 'failure.dart';

class UnknwonFailure extends Failure {
  UnknwonFailure() : super(UNKNOWN_ERROR_MESSAGE);
}
