import 'package:weatherist/src/core/constants/messages.dart';
import 'package:weatherist/src/domain/failures/failure.dart';

class ServerFailure extends Failure {
  ServerFailure() : super(SERVER_ERROR_MESSAGE);
}
