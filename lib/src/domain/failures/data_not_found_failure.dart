import 'package:weatherist/src/core/constants/messages.dart';
import 'package:weatherist/src/domain/failures/failure.dart';

class DataNotFoundFailure extends Failure {
  DataNotFoundFailure() : super(DATA_NOT_FOUND_MESSAGE);
}
