import 'package:weatherist/src/core/constants/messages.dart';
import 'package:weatherist/src/domain/failures/failure.dart';

class NetworkFailure extends Failure {
  NetworkFailure() : super(NETWORK_ERROR_MESSAGE);
}
