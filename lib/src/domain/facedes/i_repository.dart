import 'package:dartz/dartz.dart';
import 'package:weatherist/src/core/constants/consts.dart';
import 'package:weatherist/src/domain/entities/weather.dart';
import 'package:weatherist/src/domain/failures/failure.dart';

abstract class IRepository {
  Future<Either<Failure, WeatherDetails>> getWeatherDetails(
    DateTime date, {
    double lat = DEFAULT_LAT,
    double long = DEFAULT_LONG,
    String lang = DEFAULT_LANGUAGE,
  });
}
