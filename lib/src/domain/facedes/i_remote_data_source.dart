import 'package:weatherist/src/infrastructure/models/weather-list.dart';

abstract class IRemoteDataSource {
  Future<WeatherListModel> getNextThreeDaysWeather(
    double lat,
    double long,
    String lang,
  );
}
