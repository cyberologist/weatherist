import 'package:weatherist/src/domain/entities/weather.dart';

abstract class ILocalDataSource {
  Future<bool> checkWeatherExists(
    DateTime date,
    double lat,
    double long,
    String lang,
  );

  Future<bool> setWeatherDetails(
    WeatherDetails weatherDetails,
    double lat,
    double long,
    String lang,
  );

  Future<WeatherDetails> getWeatherByDateTime(
    DateTime date,
    double lat,
    double long,
    String lang,
  );

  Future<void> optimizeCashUntil(DateTime clearUntil);
}
