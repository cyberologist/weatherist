import 'package:get/get.dart';

class Messages extends Translations {
  @override
  Map<String, Map<String, String>> get keys => {
        'en_US': {
          NETWORK_ERROR_MESSAGE: 'a network error occured , please try again',
          SERVER_ERROR_MESSAGE:
              'an internal error occured , sorry we are trying to fix it',
          DATA_NOT_FOUND_MESSAGE:
              'an internal error occured , sorry we are trying to fix it',
          UNKNOWN_ERROR_MESSAGE: 'an unknown error occured , please contact us',
          TODAY: 'today',
          TOMOROW: 'tomorow',
          IN_TWO_DAYS: 'in two days',
          LOADING_WEATHER: 'Loading..'
        },
        'ar_AE': {
          NETWORK_ERROR_MESSAGE:
              'لقد حدث خطأ بالشبكة,  الرجاء المحاولة مرة أخرى',
          SERVER_ERROR_MESSAGE: 'لقد حدث خطأ داخلي ,نعتذر نحن نحاول اصلاحه',
          DATA_NOT_FOUND_MESSAGE: 'لقد حدث خطأ داخلي ,نعتذر نحن نحاول اصلاحه',
          UNKNOWN_ERROR_MESSAGE: 'لقد حدث خطأ غير معروف , الرجاء التواصل معنا',
          TODAY: 'اليوم',
          TOMOROW: 'غداً',
          IN_TWO_DAYS: 'بعد يومين',
          LOADING_WEATHER: 'جاري التحميل..'
        },
      };
}

const NETWORK_ERROR_MESSAGE = 'network Error';
const DATA_NOT_FOUND_MESSAGE = 'no Data Error';
const UNKNOWN_ERROR_MESSAGE = 'unknown Error';
const SERVER_ERROR_MESSAGE = 'server Error';
const ERROR = 'error';
const TODAY = 'today';
const TOMOROW = 'tomorow';
const IN_TWO_DAYS = 'in Two Days';
const LOADING_WEATHER = 'loading Weather';

const APP_NAME = "Weatherist!";
const APP_SLOGAN = "clean , simple and Efficent.";
const FROM = "from";
const DEV_COMPANY_NAME = "Maids.cc";
const MY_NAME = "Abdalrahman hatahet";
const WELCOME = "Welcome :)";
const TAP_PROMPT_PT1 = "Tap on a Date To view its  ";
const TAP_PROMPT_PT2 = 'Weather';
const WIND = 'wind';
const KM_PER_HOUR = 'km/h';
const HUMIDITY = 'humidity';
