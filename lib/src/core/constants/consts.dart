const String DEFAULT_CITY = "Moskou";
const String DEFAULT_LANGUAGE = "en";
const double DEFAULT_LAT = 55.751244;
const double DEFAULT_LONG = 37.618423;

const String DEFAULT_CNT = "30";
const LAT_QUERY_NAME = 'lat';
const LONG_QUERY_NAME = 'lon';
const LAND_QUERY_NAME = 'lang';
const CNT_QUERY_NAME = 'cnt';
const EXECLUDE_QUERY_NAME = 'execlude';
const DEFAULT_EXECLUDE = 'minutely,hourly';
const API_KEY_QUERY_NAME = 'appid';
