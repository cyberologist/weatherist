import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:hive/hive.dart';
import 'package:path_provider/path_provider.dart' as path_provider;
import 'package:weatherist/src/application/usecases/fetch_day_weather.dart';
import 'package:weatherist/src/application/usecases/fetch_three_days_dates.dart';
import 'package:weatherist/src/core/constants/config.dart';
import 'package:weatherist/src/domain/facedes/i_local_data_source.dart';
import 'package:weatherist/src/domain/facedes/i_remote_data_source.dart';
import 'package:weatherist/src/domain/facedes/i_repository.dart';
import 'package:weatherist/src/infrastructure/data/local_data_source.dart';
import 'package:weatherist/src/infrastructure/data/remote_data_source.dart';
import 'package:weatherist/src/infrastructure/data/repository.dart';
import 'package:weatherist/src/presentation/blocs/dates/dates_bloc.dart';
import 'package:weatherist/src/presentation/blocs/weather/weather_bloc.dart';

final getIt = GetIt.instance;
Future<void> init() async {
  WidgetsFlutterBinding.ensureInitialized();
  final appDocumentDir = await path_provider.getApplicationDocumentsDirectory();
  Hive.init(appDocumentDir.path);
  await Hive.openBox(WEATHER_BOX_KEY);

  // datasoures
  getIt.registerLazySingleton<IRemoteDataSource>(() => RemoteDataSource());
  getIt.registerLazySingleton<ILocalDataSource>(() => LocalDataSource());
  getIt.registerLazySingleton<IRepository>(() => Repository(getIt(), getIt()));

  //usecases
  getIt.registerFactory<FetchThreeDaysDates>(() => FetchThreeDaysDates());
  getIt.registerFactory<FetchDayWeather>(() => FetchDayWeather(getIt()));

  //blocs
  getIt.registerLazySingleton<DatesBloc>(() => DatesBloc(getIt()));
  getIt.registerLazySingleton<WeatherBloc>(() => WeatherBloc(getIt()));
}
